package s50;
import java.util.Date;
import java.util.Locale;
import java.util.Arrays;
import java.text.NumberFormat;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

public class Order {
    int id;
    String customerName;
    long price; //tổng giá tiền
    Date orderDate;
    boolean confirm; //đã xác nhận hay chưa
    String[] items; //danh sách các mặt hàng đã mua
    //khởi tạo 1 tham số
    public Order(String customerName) {
        this.id = 1;
        this.customerName = customerName;
        this.price = 120000;
        this.orderDate = new Date();
        this.confirm = true;
        this.items = new String[]{"book","pen","ruler"};
    }
    //khởi tạo với tất cả tham số
    public Order(int id, String customerName, long price, Date orderDate, boolean confirm, String[] items) {
        this.id = id;
        this.customerName = customerName;
        this.price = price;
        this.orderDate = orderDate;
        this.confirm = confirm;
        this.items = items;
    }
    //khởi tạo không tham số
    public Order() {
        this.id = 2;
        this.customerName = "Khách lẻ";
        this.price = 20000;
        this.orderDate = new Date();
        this.confirm = true;
        this.items = new String[]{"notebook"};
    }
    //khởi tạo với 3 tham số
    public Order(int id, String customerName, long price) {
        this(id, customerName, price, new Date(), true, new String[]{"ink","notebook","stapler","cissors"});
    }
    @Override
    public String toString() {
        //định dạng TCVN
        Locale.setDefault(new Locale("vi", "VN"));
        //định dạng ngày tháng
        String pattern = "dd-MMMM-yyyy HH:mm:ss.SSS";
        DateTimeFormatter defaultTimeFormatter = DateTimeFormatter.ofPattern(pattern);
        //Định dạng giá tiền
        Locale usLocale = Locale.getDefault();
        NumberFormat usNumberFormat = NumberFormat.getCurrencyInstance(usLocale);
        //trả ra chuỗi string
        return "Order [id=" + id 
        + ", customerName=" + customerName 
        + ", price=" + usNumberFormat.format(price) 
        + ", orderDate=" + defaultTimeFormatter.format(orderDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime())
                + ", confirm=" + confirm 
                + ", items=" + Arrays.toString(items) + "]";
    }
    
    
    
}
