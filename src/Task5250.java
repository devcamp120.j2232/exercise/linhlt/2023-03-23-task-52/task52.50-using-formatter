import java.util.ArrayList;
import java.util.Date;
import s50.Order;
public class Task5250 {
    public static void main(String[] args) throws Exception {
        ArrayList<Order> arrayList = new ArrayList<>();
        //khởi tạo order với các tham số khác nhau
        Order order1 = new Order();
        Order order2 = new Order("Linh");
        Order order3 = new Order(3,"Minh",80000);
        Order order4 = new Order(4, "Thanh",75000, new Date(), false, new String[]{"eraser","pencil","paper"});
        //thêm object order vào arraylist
        arrayList.add(order1);
        arrayList.add(order2);
        arrayList.add(order3);
        arrayList.add(order4);
        //in ra terminal
        for (Order order: arrayList){
            System.out.println(order.toString());
       }
    }
}
